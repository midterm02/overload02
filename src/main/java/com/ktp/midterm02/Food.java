/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.midterm02;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class Food {

    static Scanner mn = new Scanner(System.in);
    final static int hamburger = 295, spaghetti = 158, saladroll = 300, cookie = 502, BananaberrySoyMilkSmoothie = 198, brownie = 466;
    private static int sumcalories;
    private static int sumprice;

    private static void menu() {
        System.out.println("One dish price 30 baht");
        System.out.println("1.hamburger 295 calories");
        System.out.println("2.spaghetti 158 calories");
        System.out.println("3.saladroll 300 calories");
        System.out.println("4.cookie 502 calories");
        System.out.println("5.BananaberrySoyMilkSmoothie 198 calories");
        System.out.println("6.brownie 466 calories");
    }
    public static void main(String[] args) {
        System.out.println("Please input num :");
        int amount = mn.nextInt();
        switch (amount) {
            case 1:
                loop1();
                break;
            case 2:
                loop2();
                break;
            case 3:
                loop3();
                break;
            case 4:
                loop4();
                break;
            case 5:
                loop5();
                break;
            case 6:
                loop6();
                break;
            default:
                break;
        }    
    }
    
    private static void loop1(){
        int chr;
        menu();
        chr = mn.nextInt();
        food(Choose(chr));
        System.out.printf("All calories : %d\n", sumcalories);
        System.out.printf("Total Price : %d\n", sumprice);
    }
    private static void loop2(){
        int chs;
        String[] menu = new String[2];
        for (String i = "" ; i.length() < 2 ; i += "0"){
            menu();
            chs = mn.nextInt();
            menu[i.length()] = Choose(chs);
        }
        food(menu[0],menu[1]);
        System.out.printf("All calories: %d\n", sumcalories);
        System.out.printf("Total Price : %d\n", sumprice);
    }
    private static void loop3(){
        int chs;
        String[] menu = new String[3];
        for (String i = "" ; i.length() < 3 ; i += "0"){
            menu();
            chs = mn.nextInt();
            menu[i.length()] = Choose(chs);
        }
        food(menu[0],menu[1],menu[2]);
        System.out.printf("All calories: %d\n", sumcalories);
        System.out.printf("Total Price : %d\n", sumprice);
    }
    private static void loop4(){
        int chs;
        String[] menu = new String[4];
        for (String i = "" ; i.length() < 4 ; i += "0"){
            menu();
            chs = mn.nextInt();
            menu[i.length()] = Choose(chs);
        }
        food(menu[0],menu[1],menu[2],menu[3]);
        System.out.printf("All calories: %d\n", sumcalories);
        System.out.printf("Total Price : %d\n", sumprice);
    }
    private static void loop5(){
        int chs;
        String[] menu = new String[5];
        for (String i = "" ; i.length() < 5 ; i += "0"){
            menu();
            chs = mn.nextInt();
            menu[i.length()] = Choose(chs);
        }
        food(menu[0],menu[1],menu[2],menu[3],menu[4]);
        System.out.printf("All calories: %d\n", sumcalories);
        System.out.printf("Total Price : %d\n", sumprice);
    }
    private static void loop6(){
        int chs;
        String[] menu = new String[6];
        for (String i = "" ; i.length() < 6 ; i += "0"){
            menu();
            chs = mn.nextInt();
            menu[i.length()] = Choose(chs);
        }
        food(menu[0],menu[1],menu[2],menu[3],menu[4],menu[5]);
        System.out.printf("All calories: %d\n", sumcalories);
        System.out.printf("Total Price : %d\n", sumprice);
    }
     
      
    


    private static String Choose(int mn) {
        switch (mn) {
            case 1:
                sumcalories += hamburger;
                return "hamburger";
            case 2:
                sumcalories += spaghetti;
                return "spaghetti";
            case 3:
                sumcalories += saladroll;
                return "saladroll";
            case 4:
                sumcalories += cookie;
                return "cookie";
            case 5:
                sumcalories += BananaberrySoyMilkSmoothie;
                return "BananaberrySoyMilkSmoothie";
            case 6:
                sumcalories += brownie;
                return "brownie";
            default:
                return null;
        }
    }
    private static void food(String menu){
        sumprice += 30; 
    }
    private static void food(String menu1,String menu2){
        sumprice += 60; 
    }
    private static void food(String menu1,String menu2,String menu3){
        sumprice += 90; 
    }
    private static void food(String menu1,String menu2,String menu3,String menu4){
        sumprice += 120; 
    }
    private static void food(String menu1,String menu2,String menu3,String menu4,String menu5){
        sumprice += 150; 
    }
    private static void food(String menu1,String menu2,String menu3,String menu4,String menu5,String menu6){
        sumprice += 180; 
    }
    
}

